package com.epam.engx.story.impl;

import com.epam.engx.story.GarageCleaningService;
import com.epam.engx.story.GarageService;
import com.epam.engx.story.exception.FreeGarageIsNotFoundException;
import com.epam.engx.story.model.Car;

import java.util.HashMap;
import java.util.Map;

public class GarageServiceImpl implements GarageService {

    static final int[] SECURE_GARAGES = {1, 7}; 
    static final int[] SIMPLE_GARAGES = {2, 3, 4, 5, 6};

    private final GarageCleaningService garageCleaningService;

    private Map<Integer, Car> garagesWithCar = new HashMap<>();

    public GarageServiceImpl(GarageCleaningService garageCleaningService) {
        this.garageCleaningService = garageCleaningService;
    }

    @Override
    public int registerInGarage(Car car) throws FreeGarageIsNotFoundException {
        Integer garage;
        if (car.isClassic()) {
            garage = findFreeGarageFrom(SECURE_GARAGES);
        } else {
            garage = findFreeGarageFrom(SIMPLE_GARAGES);
        }
        if (garage != null) {
            clean(garage);
            registerCar(garage, car);
            return garage;
        } else {
            throw new FreeGarageIsNotFoundException();
        }
    }

    private void clean(int garage) {
        garageCleaningService.clean(garage);
    }

    private void registerCar(int garage, Car car) {
        garagesWithCar.put(garage, car);
    }

    private Integer findFreeGarageFrom(int[] garages) {
        for (int garage : garages) {
            if (isGarageFree(garage)) return garage;
        }
        return null;
    }

    private boolean isGarageFree(int garage) {
        return garagesWithCar.get(garage) == null;
    }

    void setGaragesWithCar(Map<Integer, Car> garagesWithCar) {
        this.garagesWithCar = garagesWithCar;
    }
}
